output "instance_sns_anr" {
    value = aws_sns_topic.message_updates.arn
}

output "instance_sns_id" {
    value = aws_sns_topic.message_updates.id
}

output "instance_sns_name" {
    value = aws_sns_topic.message_updates.name
}
