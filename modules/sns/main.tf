resource "aws_sns_topic" "message_updates" {
    name = "message-updates-topic"
    delivery_policy = file("${path.root}/policies/SNSDeliveryPolicy.json")
}