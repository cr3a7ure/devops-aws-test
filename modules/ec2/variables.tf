variable instance_type {
    type = string
    description = "Instance type"
}

variable redrive_config {
    type = string
    default = "/configs/redrive-config.json"
    description = "AWSRedrive configuration file"
}

variable pem_file {
    type = string
    # default = "./cr3a7ure.pem"
}

variable key_name {
    type = string
}

variable ami {
    type = string
    description = "EC2 ami"
}

variable security_groups {
    type = list
}

variable tags {
    type = map
}