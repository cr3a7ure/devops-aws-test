resource "aws_iam_role" "sqs_exec" {
    name = "sqs_exec"
    path = "/"
    assume_role_policy = file("${path.root}/policies/AssumeRolePolicy.json")
}

resource "aws_iam_instance_profile" "sqs_profile" {
    name = "sqs_profile"
    role = aws_iam_role.sqs_exec.name
}

resource "aws_iam_policy" "policy" {
    name = "test-policy"
    path = "/"
    description = "Test boy"
    policy = file("${path.root}/policies/RedriveEC2InstancePolicy.json")
}

resource "aws_iam_policy_attachment" "test-attach" {
  name       = "test-attachment"
  roles      = [aws_iam_role.sqs_exec.name]
  policy_arn = aws_iam_policy.policy.arn
}

resource "aws_instance" "redrive" {
    ami = var.ami
    instance_type = var.instance_type
    security_groups = var.security_groups
    key_name = var.key_name
    iam_instance_profile = aws_iam_instance_profile.sqs_profile.name
    tags = var.tags

    provisioner "file" {
        source      = "${path.root}/scripts/redrive.sh"
        destination = "/tmp/redrive.sh"
        connection {
            type     = "ssh"
            user     = "ec2-user"
            private_key = file("${var.pem_file}")
            host     = self.public_ip
        }
    }


    provisioner "file" {
        source      = "${path.root}/scripts/redrive.service"
        destination = "/tmp/redrive.service"
        connection {
            type     = "ssh"
            user     = "ec2-user"
            private_key = file("${var.pem_file}")
            host     = self.public_ip
        }
    }

    provisioner "file" {
        source      = "${path.root}${var.redrive_config}"
        destination = "/tmp/redrive-config.json"
        connection {
            type     = "ssh"
            user     = "ec2-user"
            private_key = file("${var.pem_file}")
            host     = self.public_ip
        }
    }
    provisioner "file" {
        source      = "${path.root}/configs/watch-agent-basic.json"
        destination = "/tmp/watch-agent-basic.json"
        connection {
            type     = "ssh"
            user     = "ec2-user"
            private_key = file("${var.pem_file}")
            host     = self.public_ip
        }
    }
    provisioner "file" {
        source      = "${path.root}/configs/redrive-logs.json"
        destination = "/tmp/redrive-logs.json"
        connection {
            type     = "ssh"
            user     = "ec2-user"
            private_key = file("${var.pem_file}")
            host     = self.public_ip
        }
    }

    provisioner "remote-exec" {
        connection {
            type     = "ssh"
            user     = "ec2-user"
            private_key = file("${var.pem_file}")
            host     = self.public_ip
        }
        inline = [
            "chmod +x /tmp/redrive.sh",
            "/tmp/redrive.sh",
        ]
    }
}
