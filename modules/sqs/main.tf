resource "aws_sqs_queue" "message_updates_queue" {
  name = "message-updates-queue"
  provisioner "local-exec" {
      command = <<EOT
      jq '.QueueUrl = "${self.id}"' ${path.root}${var.redrive_default_config} > tmp.$$.json && mv tmp.$$.json ${path.root}${var.redrive_default_config}
      jq '.Region = "${var.region}"' ${path.root}${var.redrive_default_config} > tmp.$$.json && mv tmp.$$.json ${path.root}${var.redrive_default_config}
      jq '. = []' ${path.root}${var.redrive_config} > temp.json && mv temp.json ${path.root}${var.redrive_config}
      jq '. += [input]' ${path.root}${var.redrive_config} ${path.root}${var.redrive_default_config} > temp.json && mv temp.json ${path.root}${var.redrive_config}
      EOT
    }
}

resource "aws_sns_topic_subscription" "message_updates_sqs_target" {
  topic_arn = var.instance_sns_anr
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.message_updates_queue.arn
}

resource "aws_sqs_queue_policy" "redrive_policy" {
  queue_url = aws_sqs_queue.message_updates_queue.id
  policy = file("${path.root}/policies/AssumeRolePolicy.json")
}