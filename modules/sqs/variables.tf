variable instance_sns_anr {
    type = string
    description = "SNS instance in use"
}

variable redrive_default_config {
    type = string
    default = "/configs/redrive-default-config.json"
    description = "AWSRedrive template configuration file"
}

variable redrive_config {
    type = string
    default = "/configs/redrive-config.json"
    description = "AWSRedrive configuration file"
}

variable pem_file {
    type = string
    default = "./default.pem"
}

variable region {
    type = string
}
