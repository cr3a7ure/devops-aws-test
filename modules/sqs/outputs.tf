
output "instance_sqs_url" {
    value = aws_sqs_queue.message_updates_queue.id
}

output "instance_sqs_arn" {
    value = aws_sqs_queue.message_updates_queue.arn
}

# output "sqs_region" {
#     value = var.region
# }
#   queue_url = "${aws_sqs_queue.q.id}"
