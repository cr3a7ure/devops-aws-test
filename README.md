# Redrive Deployment

## Symmary
Dev ops test case.
Deploy custom application on AWS utilizing a series of SNS/SQS services

## Prerequisite Requirements
The one and only requirement for execution (except terraform binary) is [jq](https://stedolan.github.io/jq/).
It is used to create the Redrive configuration file before uploading.

## Design Decisions
According to specifications and bonus points a series of decisions have been made
in order to achieve highest possible automation and tidyness.
Each environment has its own directory containing specific configureation files.


### Directory Structure
Main directory contains a series of environments and
the develoed modules. We could use a parent directory
 named *environments* to contain them, but I chose to flatten
 the directory structure.

 Inside each *env* there is the `main.tf` initiation file, along with the
 `variables.tf` and `output.tf`.

|Directory       | Description
|----------------|:----------------
| modules        | modules created for the task
| default        | default configuration structure
| *dev*          | development environment
| *prod*         | production environment
| *env*/policies | policies per environment
| *env*/scripts  | scripts (target machine) per environment
| *env*/configs  | configs (target machine) per environment


#### Modules
- ec2: EC2 instance for *Redrive* deployment
- sqs: SQS Queue creation module
- sns: SNS specific module


### Automation
A series of scripts and configuration files has been created in order
to automate as much as possible the desired deployment.
All required configurations are inside *env* directory.

|Configuration Files                           | Description
|----------------------------------------------|:----------------
| *env*/policies/SNSDeliveryPolicy.json        | SNS Blueprint
| *env*/policies/RedriveEC2InstancePolicy.json | Redrice Instance Policies
| *env*/policies/RedriveSQSQueuePolicy.json    | SQS Queue Policies
| *env*/scripts/redrive.sh                     | *Redrive.Console* deploy script
| *env*/scripts/redrive.service                | *Redrive.Console* service description
| *env*/configs/redrive-config.json            | generated redrive config
| *env*/variables.tf                           | required `.pem` and `region`

Using those config files, we are able to create a series of environments
and deploy our application using the terraform default procedure:

```bash
terraform init ./dev
terraform plan ./dev
terraform apply ./dev
```


### Environments
A series of environments can be used by selecting the appropriate directory
 and changing workspace.

### Monitoring
CloudWatch setup and configuration is used in order to support
monitoring of the *Redrive.Console* and EC2 instance in general.

### Blueprints
SNS/SQS blueprints are kept inside `*env*/policies`.