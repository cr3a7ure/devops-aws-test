#!/usr/bin/bash

sudo exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
cd /home/$(whoami)
echo "Downloading AWS Redrive"
wget https://github.com/nickntg/awsredrive.core/releases/download/1.0/awsredrive.core.linux.zip
echo "Downloading cloudWatch"
wget https://s3.amazonaws.com/amazoncloudwatch-agent/amazon_linux/amd64/latest/amazon-cloudwatch-agent.rpm

echo "Setting up AWSRedrive.Console"
unzip awsredrive.core.linux.zip -d awsredrive.core.linux
cd awsredrive.core.linux
chmod +x AWSRedrive.console
rm config.json
mv /tmp/redrive-config.json config.json
touch awsredrive.log

# should be better
nohup ./AWSRedrive.console 0<&- &> awsredrive.log &
echo "AWSRedrive.Console up and running"
cd ..
echo "Installing Amazon CloudWatch"
sudo rpm -U ./amazon-cloudwatch-agent.rpm
echo "Loading Amazon CloudWatch Configuration"
sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -c file:/tmp/watch-agent-basic.json -s
sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a append-config -m ec2 -c file:/tmp/redrive-logs.json -s
echo "Script execution ended successfully"
