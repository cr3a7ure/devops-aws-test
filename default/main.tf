provider "aws" {
    region   = "eu-central-1"
    version = "~> 2.51"
    alias = "europe"
    shared_credentials_file = "${path.cwd}${var.aws_shared_credentials_file}"
}

module "sns" {
    source = "../modules/sns"
    providers = {
        aws = aws.europe
    }
}

module "sqs" {
    source = "../modules/sqs"
    providers = {
        aws = aws.europe
    }
    instance_sns_anr = module.sns.instance_sns_anr
    region = var.region
}

module "ec2" {
    source = "../modules/ec2"
    providers = {
        aws = aws.europe
    }
    pem_file = "${path.cwd}${var.pem_file}"
    key_name = var.key_name
    instance_type = var.instance_type
    ami = var.ami
    security_groups = var.security_groups
    tags = var.redrive_tags
}
