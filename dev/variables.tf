variable region {
    type = string
    default = "eu-central-1"
    description = "Region to work on"
}

variable aws_shared_credentials_file {
    type = string
    default = "/credentials"
    description = "Default credentials file"
}

variable aws_alias {
    type = string
    default = "europe"
    description = "default region's alias"
}

variable pem_file {
    type = string
    default = "/cr3a7ure-default.pem"
    description = "PEM file required for remote exec"
}

variable key_name {
    type = string
    default = "cr3a7ure-default"
    description = "Default key for EC2 instance."
}

variable ami {
    type = string
    default = "ami-0f7a4c9d36399c73f"
    description = "EC2 ami"
}

variable security_groups {
    default = [ "allowremote" ]
}

variable instance_type {
    type = string
    default = "t2.micro"
    description = "Instance type"
}

variable redrive_tags {
    type = map
    default = {
        Name = "Redrive instance"
        Environment = "testing"
    }
}