output env_path {
    value = path.root
}

output PEM_file {
    value = pathexpand("../cr3a7ure-default.pem")
}